import { Region } from "./regions";

type Currency = {
  code: string;
  name: string;
  symbol: string;
};

type Language = {
  iso639_1: string;
  iso639_2: string;
  name: string;
  nativeName: string;
};

export type Country = {
  name: string;
  nativeName: string;
  capital: string;
  region: Region;
  subregion: string;
  population: number;
  flag: string;
  flags: {
    svg: string;
    png: string;
  };
  topLevelDomain: string[];
  independent: boolean;
  borders: string[];
  currencies: Currency[];
  languages: Language[];
};

type Info = {
  title: string;
  description: string;
};

export type CountryInfo = {
  img: string;
  name: string;
  info: Info[];
};
