import { useSelector } from "react-redux";
import { SingleValue } from "react-select";
import { selectRegion } from "./controls-selectors";
import { setRegion } from "./controls-slice";
import { Region } from "../../types";
import { useAppDispatch } from "../../store";
import { SelectOptions } from "./CustomSelect";

type handleSelectType = (reg: SingleValue<SelectOptions>) => void;

export const useRegion = (): [Region | "", handleSelectType] => {
  const dispatch = useAppDispatch();
  const region = useSelector(selectRegion);

  const handleSelect: handleSelectType = (reg) => {
    if (reg) {
      dispatch(setRegion(reg.value));
    } else {
      dispatch(setRegion(""));
    }
  };

  return [region, handleSelect];
};
