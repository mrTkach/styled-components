import { ChangeEventHandler } from "react";
import { useSelector } from "react-redux";
import { selectSearch } from "./controls-selectors";
import { setSearch } from "./controls-slice";
import { useAppDispatch } from "../../store";

type onSearchType = ChangeEventHandler<HTMLInputElement>;

export const useSearch = (): [string, onSearchType] => {
  const dispatch = useAppDispatch();
  const search = useSelector(selectSearch);

  const handleSearch: onSearchType = (e) => {
    const target = e.target;
    dispatch(setSearch(target.value));
  };

  return [search, handleSearch];
};
