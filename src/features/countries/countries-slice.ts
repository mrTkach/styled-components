import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { Status, Country, Extra } from "../../types/";

export const loadCountries = createAsyncThunk<
  // describing 3 parameters of createAsyncThunk:
  // axios return object, that's why should be { data: Country[] }
  { data: Country[] }, // what we return (Returned)
  undefined, // incoming parameter if any (ThunkArg)
  {
    state: { countries: CountrySlice };
    extra: Extra; // , (ThunkApiConfig)
    rejectValue: string; // именно rejectValue, а не rejectWithValue
  }
>(
  "@@countries/load-countries",
  async (_, { extra: { client, api }, rejectWithValue }) => {
    try {
      return client.get(api.ALL_COUNTRIES);
    } catch (error) {
      if (error instanceof Error) {
        return rejectWithValue(error.message);
      }
      return rejectWithValue("Unknown error");
    }
  },
  {
    // condition needs to avoid running actions in dev mode twice
    condition: (_, { getState }) => {
      const {
        countries: { status },
      } = getState();

      if (status === "loading") {
        return false;
      }
    },
  }
);

type CountrySlice = {
  status: Status;
  error: null | string;
  list: Country[];
};

const initialState: CountrySlice = {
  status: "idle",
  error: null,
  list: [],
};

const countrySlice = createSlice({
  name: "@@countries",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(loadCountries.pending, (state) => {
        state.status = "loading";
        state.error = null;
      })
      .addCase(loadCountries.rejected, (state, action) => {
        state.status = "rejected";
        state.error = action.payload || "Cannot load data";
      })
      .addCase(loadCountries.fulfilled, (state, action) => {
        state.status = "received";
        state.list = action.payload.data;
      });
  },
});

export const countryReducer = countrySlice.reducer;
