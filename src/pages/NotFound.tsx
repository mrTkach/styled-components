import { Link } from "react-router-dom";

export const NotFound = () => {
  return (
    <>
      <div>This page doesn't exist</div>
      <Link to="/styled-components">Home</Link>
    </>
  );
};
